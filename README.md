# IPv4 CPP
A simple, still WIP C++ class that implements IPv4 addresses.
A test program is included.

* * *
## Compilation instructions
No external libraries should be required other than the C++ standard library.

`g++ Ipv4.cpp main.cpp -o ip`

## Executing the program
`./ip [List of IP addresses...]`

If no subnet mask is provided (by CIDR notation), it will be guessed automatically
based on the address's class.