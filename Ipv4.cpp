#include "Ipv4.h"
#include <string_view>

namespace dom {
    Ipv4::Ipv4(uint8_t first, uint8_t second, uint8_t third, uint8_t fourth, uint8_t cidr) noexcept
        : fourth(fourth), third(third), second(second), first(first),
          cidr((cidr != 0) ? cidr : Ipv4::cidr_from_value(first))
    {}

    Ipv4::Ipv4(uint32_t address, uint8_t cidr) noexcept
        : address(address),
          cidr((cidr != 0) ? cidr : Ipv4::cidr_from_value(address >> 24))
    {}

    // MEMBER FUNCTIONS
    Ip_class Ipv4::getClass() const noexcept
    {
        if      (first == Ip_class::Broadcast)        return Ip_class::Broadcast;
        else if((first & Ip_class::E) == Ip_class::E) return Ip_class::E;
        else if((first & Ip_class::D) == Ip_class::D) return Ip_class::D;
        else if((first & Ip_class::C) == Ip_class::C) return Ip_class::C;
        else if((first & Ip_class::B) == Ip_class::B) return Ip_class::B;
        else                                          return Ip_class::A;
    }

    uint32_t Ipv4::getMask() const noexcept
    {
        return (0xFFFFFFFF << (32 - cidr));
    }

    // STATIC METHODS
    bool Ipv4::validate_ip(std::string_view ip_string) noexcept
    {
        int cursor = 0, dot_count = 0;
        uint16_t val = 0;
        bool hasSlash = false;

        do
        {
            if(ip_string[cursor] >= '0' && ip_string[cursor] <= '9')
            {
                if(!hasSlash && dot_count >= 4)
                    return false;
                val *= 10;
                val += (uint16_t) (ip_string[cursor] - '0');
            }
            else if(ip_string[cursor] == '.' || ip_string[cursor] == '/')
            {
                if(ip_string[cursor] == '/')
                    hasSlash = true;

                dot_count++;
                if(val > 255 || dot_count > 4)
                    return false;
                else
                    val = 0;
            }
            else if(ip_string[cursor] == '\0')
            {
                if((hasSlash && val > 32) ||
                   (!hasSlash && val > 255) ||
                   (dot_count < 3 || dot_count > 4))
                    return false;
            }
            else
                return false;
        } while(ip_string[cursor++]);

        return true;
    }

    Ipv4 Ipv4::from_string(std::string_view ip_string) noexcept
    {
        uint8_t address[5] = { 0 };
        int cursor = 0, i = 0, dot_count = 0;
        bool hasSlash = false;

        do
        {
            if(ip_string[cursor] >= '0' && ip_string[cursor] <= '9')
            {
                if(!hasSlash && dot_count >= 4)
                    return Ipv4{0, 0};
                address[i] *= 10;
                address[i] += (uint8_t) (ip_string[cursor] - '0');
            }
            else if(ip_string[cursor] == '.' || ip_string[cursor] == '/')
            {
                if(ip_string[cursor] == '/')
                    hasSlash = true;
                else
                    dot_count++;

                if(address[i] > 255 || dot_count > 4)
                    return Ipv4{0, 0};
                else
                    i++;
            }
            else if(ip_string[cursor] == '\0')
            {
                if((hasSlash && address[i] > 32) ||
                   (!hasSlash && address[i] > 255) ||
                   (dot_count < 3 || dot_count > 4))
                    return Ipv4{0, 0};
            }
            else
                return Ipv4{0, 0};
        } while(ip_string[cursor++]);

        return Ipv4(address[0], address[1], address[2], address[3],
                    address[4]);
    }


    uint8_t Ipv4::cidr_from_value(uint8_t first) noexcept
    {
        if      (first == Ip_class::Broadcast)        return 32;
        else if((first & Ip_class::E) == Ip_class::E) return  4;
        else if((first & Ip_class::D) == Ip_class::D) return  4;
        else if((first & Ip_class::C) == Ip_class::C) return 24;
        else if((first & Ip_class::B) == Ip_class::B) return 16;
        else                                          return  8;
    }
}
