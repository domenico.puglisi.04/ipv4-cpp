#pragma once
#include <ostream>
#include <string_view>

namespace dom
{
    enum Ip_class : uint8_t {
        A = 0x00, //   0
        B = 0x80, // 128
        C = 0xC0, // 192
        D = 0xE0, // 224
        E = 0xF0, // 240

        Broadcast = 0xFF,
    };
 
    struct Ipv4
    {
        union {
            uint32_t address;
            struct {
                uint8_t fourth;
                uint8_t third;
                uint8_t second;
                uint8_t first;
            };
        };

        uint8_t cidr;

    public:
        Ipv4(uint8_t first, uint8_t second, uint8_t third, uint8_t fourth, uint8_t cidr = 0) noexcept;
        explicit Ipv4(uint32_t address, uint8_t cidr = 0) noexcept;

    // METHODS
    public:
        [[nodiscard]] Ip_class getClass() const noexcept;
        [[nodiscard]] uint32_t getMask()  const noexcept;


    // STATIC FUNCTIONS
    public:
        static bool    validate_ip(std::string_view ip_string) noexcept;
        static Ipv4    from_string(std::string_view ip_string) noexcept;
        static uint8_t cidr_from_value(uint8_t first) noexcept;

        friend std::ostream& operator<< (std::ostream &o, const Ipv4 &ip)
        {
            return (o << (int) ip.first  << '.'
                      << (int) ip.second << '.'
                      << (int) ip.third  << '.'
                      << (int) ip.fourth << '/'
                      << (int) ip.cidr
                    );
        }
    };
}
