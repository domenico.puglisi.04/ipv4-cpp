#include <iostream>
#include <string_view>

#include "Ipv4.h"

using dom::Ipv4;
using dom::Ip_class;

int main(int argc, char *argv[])
{
    for(int i = 1; i < argc; i++)
    {
        Ipv4 ip = Ipv4::from_string(std::string_view { argv[i] });
        uint32_t subnet_mask = ip.getMask();
        std::cout
            << ip << " ("
            << ((subnet_mask & 0xFF000000) >> 24) << '.'
            << ((subnet_mask & 0x00FF0000) >> 16) << '.'
            << ((subnet_mask & 0x0000FF00) >>  8) << '.'
            << ((subnet_mask & 0x000000FF) >>  0)
            << ")\n";

        uint32_t hosts = 0;
        std::cerr << "Please insert the no. of hosts: ";
        std::cin  >> hosts;

        Ipv4 start = ip, end = ip;
        start.address &= start.getMask();
        end.address |= ~end.getMask();

        if(hosts <= (((uint64_t) 0xFFFFFFFF & (~ip.getMask())) - 1))
            std::cout
                << "Network:   \t" << start << '\n'
                << "Broadcast: \t" << end   << '\n'
                << "First Host:\t" << Ipv4 { start.address + 1, ip.cidr }
                << '\n'
                << "Last  Host:\t" << Ipv4 { start.address + hosts, ip.cidr }
                << '\n'
                << "           \tunused hosts = "
                << ((ip.cidr == 32) ? 0 : ((((uint64_t) 1 << (32 - ip.cidr)) - 2) - hosts))
                << '\n'
                << '\n'; 
        else
            std::cout
                << "The given network cannot hold "
                << hosts << " host"
                << ((hosts == 1) ? ".\n\n" : "s.\n\n");
    }

    return 0;
}

